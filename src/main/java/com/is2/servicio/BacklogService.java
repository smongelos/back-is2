package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.BacklogDao;
import com.is2.entidad.Backlog;

public class BacklogService extends ServiceBase<Backlog, DaoBase<Backlog>> {

	@Inject
	private BacklogDao dao;

	@Override
	public DaoBase<Backlog> getDao() {
		return dao;
	}
}