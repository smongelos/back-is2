package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.EstadosDao;
import com.is2.entidad.Estados;

public class EstadosService extends ServiceBase<Estados, DaoBase<Estados>> {

	@Inject
	private EstadosDao dao;

	@Override
	public DaoBase<Estados> getDao() {
		return dao;
	}
}