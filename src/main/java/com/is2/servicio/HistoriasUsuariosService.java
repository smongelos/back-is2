package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.HistoriasUsuariosDao;
import com.is2.entidad.HistoriasUsuarios;

public class HistoriasUsuariosService extends ServiceBase<HistoriasUsuarios, DaoBase<HistoriasUsuarios>> {

	@Inject
	private HistoriasUsuariosDao dao;

	@Override
	public DaoBase<HistoriasUsuarios> getDao() {
		return dao;
	}
}