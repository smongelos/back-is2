package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.ProyectosDao;
import com.is2.entidad.Proyectos;

public class ProyectosService extends ServiceBase<Proyectos, DaoBase<Proyectos>> {

	@Inject
	private ProyectosDao dao;

	@Override
	public DaoBase<Proyectos> getDao() {
		return dao;
	}
}