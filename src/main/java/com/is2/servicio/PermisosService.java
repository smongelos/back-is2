package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.PermisosDao;
import com.is2.entidad.Permisos;

public class PermisosService extends ServiceBase<Permisos, DaoBase<Permisos>> {

	@Inject
	private PermisosDao dao;

	@Override
	public DaoBase<Permisos> getDao() {
		return dao;
	}
}