package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.SprintDao;
import com.is2.entidad.Sprint;

public class SprintService extends ServiceBase<Sprint, DaoBase<Sprint>> {

	@Inject
	private SprintDao dao;

	@Override
	public DaoBase<Sprint> getDao() {
		return dao;
	}
}