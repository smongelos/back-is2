package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.UsuariosDao;
import com.is2.entidad.Usuarios;

public class UsuariosService extends ServiceBase<Usuarios, DaoBase<Usuarios>> {

	@Inject
	private UsuariosDao dao;

	@Override
	public DaoBase<Usuarios> getDao() {
		return dao;
	}
}