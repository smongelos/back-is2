package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.RolesDao;
import com.is2.entidad.Roles;

public class RolesService extends ServiceBase<Roles, DaoBase<Roles>> {

	@Inject
	private RolesDao dao;

	@Override
	public DaoBase<Roles> getDao() {
		return dao;
	}
}