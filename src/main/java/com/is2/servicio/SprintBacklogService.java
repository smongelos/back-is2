package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.SprintBacklogDao;
import com.is2.entidad.SprintBacklog;

public class SprintBacklogService extends ServiceBase<SprintBacklog, DaoBase<SprintBacklog>> {

	@Inject
	private SprintBacklogDao dao;

	@Override
	public DaoBase<SprintBacklog> getDao() {
		return dao;
	}
}