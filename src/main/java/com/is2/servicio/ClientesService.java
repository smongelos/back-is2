package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.ClientesDao;
import com.is2.entidad.Clientes;

public class ClientesService extends ServiceBase<Clientes, DaoBase<Clientes>> {

	@Inject
	private ClientesDao dao;

	@Override
	public DaoBase<Clientes> getDao() {
		return dao;
	}
}