package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.ProyectosAsignadosDao;
import com.is2.entidad.ProyectosAsignados;

public class ProyectosAsignadosService extends ServiceBase<ProyectosAsignados, DaoBase<ProyectosAsignados>> {

	@Inject
	private ProyectosAsignadosDao dao;

	@Override
	public DaoBase<ProyectosAsignados> getDao() {
		return dao;
	}
}