package com.is2.servicio;

import javax.inject.Inject;
import com.is2.util.DaoBase;
import com.is2.util.ServiceBase;
import com.is2.dao.PermisosAsignadosDao;
import com.is2.entidad.PermisosAsignados;

public class PermisosAsignadosService extends ServiceBase<PermisosAsignados, DaoBase<PermisosAsignados>> {

	@Inject
	private PermisosAsignadosDao dao;

	@Override
	public DaoBase<PermisosAsignados> getDao() {
		return dao;
	}
}