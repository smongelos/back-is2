package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "sprint", schema = "public")
public class Sprint extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "SPRINT_ID_SPRINT_SEQ", allocationSize=1, initialValue=1, sequenceName = "sprint_sprint_id_seq")	
	@GeneratedValue(generator = "SPRINT_ID_SPRINT_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "sprint_id")
	private Integer sprintId;

	@Column(name = "fecha_inicio")
	private Date fechaInicio;

	@Column(name = "fecha_fin")
	private Date fechaFin;

	@Column(name = "sprint_description")
	private String sprintDescription;


	public Integer getSprintId(){
		return sprintId;
	}
	public void setSprintId(Integer sprintId){
		this.sprintId = sprintId;
	}
	public Date getFechaInicio(){
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio){
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin(){
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin){
		this.fechaFin = fechaFin;
	}
	public String getSprintDescription(){
		return sprintDescription;
	}
	public void setSprintDescription(String sprintDescription){
		this.sprintDescription = sprintDescription;
	}

}