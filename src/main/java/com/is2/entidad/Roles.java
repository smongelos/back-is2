package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "roles", schema = "public")
public class Roles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "ROLES_ID_ROL_SEQ", allocationSize=1, initialValue=1, sequenceName = "roles_id_rol_seq")	
	@GeneratedValue(generator = "ROLES_ID_ROL_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_roles")
	private Integer idRoles;

	@Column(name = "rol_descripcion")
	private String rolDescripcion;


	public Integer getIdRoles(){
		return idRoles;
	}
	public void setIdRoles(Integer idRoles){
		this.idRoles = idRoles;
	}
	public String getRolDescripcion(){
		return rolDescripcion;
	}
	public void setRolDescripcion(String rolDescripcion){
		this.rolDescripcion = rolDescripcion;
	}

}