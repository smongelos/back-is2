package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "backlog", schema = "public")
public class Backlog extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "BACKLOG_ID_BACKLOG_SEQ", allocationSize=1, initialValue=1, sequenceName = "backlog_id_backlog_seq")	
	@GeneratedValue(generator = "BACKLOG_ID_BACKLOG_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_backlog")
	private Integer idBacklog;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "id_historias_usuarios")
	private Integer idHistoriasUsuarios;


	public Integer getIdBacklog(){
		return idBacklog;
	}
	public void setIdBacklog(Integer idBacklog){
		this.idBacklog = idBacklog;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public Integer getIdHistoriasUsuarios(){
		return idHistoriasUsuarios;
	}
	public void setIdHistoriasUsuarios(Integer idHistoriasUsuarios){
		this.idHistoriasUsuarios = idHistoriasUsuarios;
	}

}