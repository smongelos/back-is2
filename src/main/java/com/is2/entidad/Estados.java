package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "estados", schema = "public")
public class Estados extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "ESTADOS_ID_ESTADO_SEQ", allocationSize=1, initialValue=1, sequenceName = "estados_id_estado_seq")	
	@GeneratedValue(generator = "ESTADOS_ID_ESTADO_SEQ", strategy=GenerationType.SEQUENCE)
	

	@Id
	@Column(name = "id_estados")
	private Integer idEstados;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "estado")
	private String estado;


	public Integer getIdEstados(){
		return idEstados;
	}
	public void setIdEstados(Integer idEstados){
		this.idEstados = idEstados;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getEstado(){
		return estado;
	}
	public void setEstado(String estado){
		this.estado = estado;
	}

}