package com.is2.entidad;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.is2.util.EntityBase;

@Entity
@Table(name = "permisos", schema = "public")
public class Permisos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PERMISOS_ID_PERMISO_SEQ", allocationSize=1, initialValue=1, sequenceName = "permisos_id_permiso_seq")	
	@GeneratedValue(generator = "PERMISOS_ID_PERMISO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_permisos")
	private Integer idPermisos;

	@Column(name = "nombre")
	private String nombre;


	public Integer getIdPermisos(){
		return idPermisos;
	}
	public void setIdPermisos(Integer idPermisos){
		this.idPermisos = idPermisos;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}

}