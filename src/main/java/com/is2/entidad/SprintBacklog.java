package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "sprint_backlog", schema = "public")
public class SprintBacklog extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_sprint_backlog")
	private Integer idSprintBacklog;

	@Column(name = "sprint_id")
	private Integer sprintId;

	@Column(name = "id_backlog")
	private Integer idBacklog;

	@Column(name = "id_estados")
	private Integer idEstados;


	public Integer getIdSprintBacklog(){
		return idSprintBacklog;
	}
	public void setIdSprintBacklog(Integer idSprintBacklog){
		this.idSprintBacklog = idSprintBacklog;
	}
	public Integer getSprintId(){
		return sprintId;
	}
	public void setSprintId(Integer sprintId){
		this.sprintId = sprintId;
	}
	public Integer getIdBacklog(){
		return idBacklog;
	}
	public void setIdBacklog(Integer idBacklog){
		this.idBacklog = idBacklog;
	}
	public Integer getIdEstados(){
		return idEstados;
	}
	public void setIdEstados(Integer idEstados){
		this.idEstados = idEstados;
	}

}