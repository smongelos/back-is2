package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "proyectos_asignados", schema = "public")
public class ProyectosAsignados extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PROYECTOS_ASIGNADOS_ID_PROYECTOS_ASIGNADOS_SEQ", allocationSize=1, initialValue=1, sequenceName = "proyectos_asignados_id_proyectos_asignados_seq")	
	@GeneratedValue(generator = "PROYECTOS_ASIGNADOS_ID_PROYECTOS_ASIGNADOS_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_proyectos_asignados")
	private Integer idProyectosAsignados;

	@Column(name = "id_proyectos")
	private Integer idProyectos;

	@Column(name = "id_usuarios")
	private Integer idUsuarios;

	@Column(name = "id_roles")
	private Integer idRoles;


	public Integer getIdProyectosAsignados(){
		return idProyectosAsignados;
	}
	public void setIdProyectosAsignados(Integer idProyectosAsignados){
		this.idProyectosAsignados = idProyectosAsignados;
	}
	public Integer getIdProyectos(){
		return idProyectos;
	}
	public void setIdProyectos(Integer idProyectos){
		this.idProyectos = idProyectos;
	}
	public Integer getIdUsuarios(){
		return idUsuarios;
	}
	public void setIdUsuarios(Integer idUsuarios){
		this.idUsuarios = idUsuarios;
	}
	public Integer getIdRoles(){
		return idRoles;
	}
	public void setIdRoles(Integer idRoles){
		this.idRoles = idRoles;
	}

}