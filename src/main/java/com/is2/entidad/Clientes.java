package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "clientes", schema = "public")
public class Clientes extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "CLIENTE_ID_USUARIO_SEQ_1", allocationSize=1, initialValue=1, sequenceName = "cliente_id_usuario_seq_1")	
	@GeneratedValue(generator = "CLIENTE_ID_USUARIO_SEQ_1", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_clientes")
	private Integer idClientes;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "correo")
	private String correo;


	public Integer getIdClientes(){
		return idClientes;
	}
	public void setIdClientes(Integer idClientes){
		this.idClientes = idClientes;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getCorreo(){
		return correo;
	}
	public void setCorreo(String correo){
		this.correo = correo;
	}

}