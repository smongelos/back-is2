package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "proyectos", schema = "public")
public class Proyectos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PROYECTOS_ID_PROYECTO_SEQ", allocationSize=1, initialValue=1, sequenceName = "proyectos_id_proyecto_seq")	
	@GeneratedValue(generator = "PROYECTOS_ID_PROYECTO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_proyectos")
	private Integer idProyectos;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "alias")
	private String alias;

	@Column(name = "fecha_inicio")
	private Date fechaInicio;

	@Column(name = "fecha_fin")
	private Date fechaFin;

	@Column(name = "id_estados")
	private Integer idEstados;

	@Column(name = "id_clientes")
	private Integer idClientes;


	public Integer getIdProyectos(){
		return idProyectos;
	}
	public void setIdProyectos(Integer idProyectos){
		this.idProyectos = idProyectos;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getAlias(){
		return alias;
	}
	public void setAlias(String alias){
		this.alias = alias;
	}
	public Date getFechaInicio(){
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio){
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin(){
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin){
		this.fechaFin = fechaFin;
	}
	public Integer getIdEstados(){
		return idEstados;
	}
	public void setIdEstados(Integer idEstados){
		this.idEstados = idEstados;
	}
	public Integer getIdClientes(){
		return idClientes;
	}
	public void setIdClientes(Integer idClientes){
		this.idClientes = idClientes;
	}

}