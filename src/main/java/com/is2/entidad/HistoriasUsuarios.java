package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "historias_usuarios", schema = "public")
public class HistoriasUsuarios extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "HISTORIAS_USUARIOS_ID_HISTORIAS_USUARIOS_SEQ_1", allocationSize=1, initialValue=1, sequenceName = "historias_usuarios_id_historias_usuarios_seq_1")	
	@GeneratedValue(generator = "HISTORIAS_USUARIOS_ID_HISTORIAS_USUARIOS_SEQ_1", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_historias_usuarios")
	private Integer idHistoriasUsuarios;

	@Column(name = "estado")
	private String estado;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "id_proyectos")
	private Integer idProyectos;


	public Integer getIdHistoriasUsuarios(){
		return idHistoriasUsuarios;
	}
	public void setIdHistoriasUsuarios(Integer idHistoriasUsuarios){
		this.idHistoriasUsuarios = idHistoriasUsuarios;
	}
	public String getEstado(){
		return estado;
	}
	public void setEstado(String estado){
		this.estado = estado;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public Integer getIdProyectos(){
		return idProyectos;
	}
	public void setIdProyectos(Integer idProyectos){
		this.idProyectos = idProyectos;
	}

}