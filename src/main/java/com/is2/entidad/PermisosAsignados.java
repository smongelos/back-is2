package com.is2.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.is2.util.EntityBase;

@Entity
@Table(name = "permisos_asignados", schema = "public")
public class PermisosAsignados extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "PERMISOS_ASIGNADOS_ID_PERMISO_ASIGNADO_SEQ", allocationSize=1, initialValue=1, sequenceName = "permisos_asignados_id_permisos_asignados_seq")	
	@GeneratedValue(generator = "PERMISOS_ASIGNADOS_ID_PERMISO_ASIGNADO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_permisos_asignados")
	private Integer idPermisosAsignados;

	@Column(name = "id_roles")
	private Integer idRoles;

	@Column(name = "id_permisos")
	private Integer idPermisos;


	public Integer getIdPermisosAsignados(){
		return idPermisosAsignados;
	}
	public void setIdPermisosAsignados(Integer idPermisosAsignados){
		this.idPermisosAsignados = idPermisosAsignados;
	}
	public Integer getIdRoles(){
		return idRoles;
	}
	public void setIdRoles(Integer idRoles){
		this.idRoles = idRoles;
	}
	public Integer getIdPermisos(){
		return idPermisos;
	}
	public void setIdPermisos(Integer idPermisos){
		this.idPermisos = idPermisos;
	}

}