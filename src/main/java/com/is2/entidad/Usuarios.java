package com.is2.entidad;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.is2.util.EntityBase;

@Entity
@Table(name = "usuarios", schema = "public")
public class Usuarios extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	@SequenceGenerator(name = "USUARIOS_ID_USUARIO_SEQ", allocationSize=1, initialValue=1, sequenceName = "usuarios_id_usuario_seq")	
	@GeneratedValue(generator = "USUARIOS_ID_USUARIO_SEQ", strategy=GenerationType.SEQUENCE)

	@Id
	@Column(name = "id_usuarios")
	private Integer idUsuarios;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "mail")
	private String mail;

	@Column(name = "pass")
	private String pass;

	@Column(name = "users")
	private String users;

	@Column(name = "telefono")
	private String telefono;


	public Integer getIdUsuarios(){
		return idUsuarios;
	}
	public void setIdUsuarios(Integer idUsuarios){
		this.idUsuarios = idUsuarios;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getMail(){
		return mail;
	}
	public void setMail(String mail){
		this.mail = mail;
	}
	public String getPass(){
		return pass;
	}
	public void setPass(String pass){
		this.pass = pass;
	}
	public String getUsers(){
		return users;
	}
	public void setUsers(String users){
		this.users = users;
	}
	public String getTelefono(){
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}

}