package com.is2.vistas.vistaProyecto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.is2.excepciones.ApplicationException;

public class VistaProyectoService {


	@PersistenceContext
	public EntityManager em;

	


	public  List<Map<String, Object>> listarProyectos() throws ApplicationException{
		try{				
				List<Object[]> postComments = null;		
				
				String query = " select p.nombre, p.alias, p.fecha_inicio, p.fecha_fin, p.id_proyectos, "
						+ " p.id_estados, s.estado from proyectos p "
						+ " join estados s on s.id_estados = p.id_estados ";


		        postComments = em.createNativeQuery(query.toString()).
		                getResultList();

		        List<Map<String, Object>> resultlist = new ArrayList<>();
		        for (Object[] oResultArray : postComments) {
		            Map<String, Object> oMapResult = new HashMap<>();
		            oMapResult.put("nombre", oResultArray[0]);
		            oMapResult.put("alias", oResultArray[1]);	
		            oMapResult.put("fechaInicio", oResultArray[2]);
		            oMapResult.put("fechaFin", oResultArray[3]);
		            oMapResult.put("idProyectos", oResultArray[4]);
		            oMapResult.put("idEstado", oResultArray[5]);
		            oMapResult.put("estado", oResultArray[6]);
		         resultlist.add(oMapResult);
		        }
		        return resultlist;
		}catch (Exception e) {
			throw new ApplicationException(e);
		}		
	}
	
	public  List<Map<String, Object>> listarHistorias() throws ApplicationException{
		try{				
				List<Object[]> postComments = null;		
				
				String query = " select h.descripcion, h.estado, "
						+ " h.id_proyectos, p.nombre from historias_usuarios h "
						+ " join proyectos p on p.id_proyectos = h.id_proyectos ";


		        postComments = em.createNativeQuery(query.toString()).
		                getResultList();

		        List<Map<String, Object>> resultlist = new ArrayList<>();
		        for (Object[] oResultArray : postComments) {
		            Map<String, Object> oMapResult = new HashMap<>();
		            oMapResult.put("descripcion", oResultArray[0]);
		            oMapResult.put("estado", oResultArray[1]);	
		            oMapResult.put("id_proyectos", oResultArray[2]);
		            oMapResult.put("nombre", oResultArray[3]);
		         resultlist.add(oMapResult);
		        }
		        return resultlist;
		}catch (Exception e) {
			throw new ApplicationException(e);
		}		
	}
	
	public  List<Map<String, Object>> listarProyectosAsignados() throws ApplicationException{
		try{				
				List<Object[]> postComments = null;	
				
				String query = "select p.nombre as nombre_proyecto, u.nombre as nombre_ususario, r.rol_descripcion , pa.id_proyectos_asignados as id_proyectosAsignados, p.id_proyectos"
						+ " from proyectos_asignados pa "
						+ " join proyectos p on p.id_proyectos = pa.id_proyectos "
						+ " join usuarios u on u.id_usuarios = pa.id_usuarios "
						+ " join roles r on r.id_roles = pa.id_roles ";


		        postComments = em.createNativeQuery(query.toString()).
		                getResultList();

		        List<Map<String, Object>> resultlist = new ArrayList<>();
		        for (Object[] oResultArray : postComments) {
		            Map<String, Object> oMapResult = new HashMap<>();
		            oMapResult.put("nombre_proyecto", oResultArray[0]);
		            oMapResult.put("nombre_usuario", oResultArray[1]);	
		            oMapResult.put("rol", oResultArray[2]);	
		            oMapResult.put("id_proyectosAsignados", oResultArray[3]);
		            oMapResult.put("id_proyectos", oResultArray[4]);
		         resultlist.add(oMapResult);
		        }
		        return resultlist;
		}catch (Exception e) {
			throw new ApplicationException(e);
		}		
	}
	
	public  List<Map<String, Object>> listarVistaBacklog() throws ApplicationException{
		try{				
				List<Object[]> postComments = null;	
				
				String query = "select b.descripcion as nombre_backlog, h.descripcion as nombre_historia"
						+ " from backlog b "
						+ " join historias_usuarios h on h.id_historias_usuarios = b.id_historias_usuarios ";


		        postComments = em.createNativeQuery(query.toString()).
		                getResultList();

		        List<Map<String, Object>> resultlist = new ArrayList<>();
		        for (Object[] oResultArray : postComments) {
		            Map<String, Object> oMapResult = new HashMap<>();
		            oMapResult.put("backlog", oResultArray[0]);
		            oMapResult.put("historias_ususarios", oResultArray[1]);	
		         resultlist.add(oMapResult);
		        }
		        return resultlist;
		}catch (Exception e) {
			throw new ApplicationException(e);
		}		
	}
	
	public  List<Map<String, Object>> listarVistaSprintBacklog() throws ApplicationException{
		try{				
				List<Object[]> postComments = null;	
				
				String query = " select b.descripcion as nombre_backlog, s.sprint_description as nombre_sprint, es.nombre as estado "
						+ " from sprint_backlog sb "
						+ " join backlog b on b.id_backlog = sb.id_backlog "
						+ " join sprint s on s.sprint_id = sb.sprint_id "
						+ " join estados es on es.id_estados = sb.id_estados ";


		        postComments = em.createNativeQuery(query.toString()).
		                getResultList();

		        List<Map<String, Object>> resultlist = new ArrayList<>();
		        for (Object[] oResultArray : postComments) {
		            Map<String, Object> oMapResult = new HashMap<>();
		            oMapResult.put("nombre_backlog", oResultArray[0]);
		            oMapResult.put("nombre_sprint", oResultArray[1]);
		            oMapResult.put("estado", oResultArray[2]);	
		         resultlist.add(oMapResult);
		        }
		        return resultlist;
		}catch (Exception e) {
			throw new ApplicationException(e);
		}		
	}
	
}