package com.is2.vistas.vistaProyecto;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Named
@RequestScoped
@Path("/rest/vista-proyecto")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VistaProyectoResource {

    @Inject
    private VistaProyectoService servicio;

    public VistaProyectoService getServicio() {
        return servicio;
    }



    @GET
    @Path("listar-proyectos")
    public Response listarProyectos() {
        List<Map<String, Object>> lista = null;
        HashMap<String, Object> response = new HashMap<String, Object>();
        lista = getServicio().listarProyectos();

        response.put("lista", lista);
        response.put("totalDatos", lista.size());

        return Response.ok(response).build();
    }
    
    @GET
    @Path("listar-historias")
    public Response listarHistorias() {
        List<Map<String, Object>> lista = null;
        HashMap<String, Object> response = new HashMap<String, Object>();
        lista = getServicio().listarHistorias();

        response.put("lista", lista);
        response.put("totalDatos", lista.size());

        return Response.ok(response).build();
    }
    
    
    @GET
    @Path("listar-proyecto-asignado")
    public Response listarProyectosAsignados() {
        List<Map<String, Object>> lista = null;
        HashMap<String, Object> response = new HashMap<String, Object>();
        lista = getServicio().listarProyectosAsignados();

        response.put("lista", lista);
        response.put("totalDatos", lista.size());

        return Response.ok(response).build();
    }
    
    @GET
    @Path("listar-vista-backlog")
    public Response listarVistaBacklog() {
        List<Map<String, Object>> lista = null;
        HashMap<String, Object> response = new HashMap<String, Object>();
        lista = getServicio().listarVistaBacklog();

        response.put("lista", lista);
        response.put("totalDatos", lista.size());

        return Response.ok(response).build();
    }
    
    @GET
    @Path("listar-vista-sprint")
    public Response listarVistaSprintBacklog() {
        List<Map<String, Object>> lista = null;
        HashMap<String, Object> response = new HashMap<String, Object>();
        lista = getServicio().listarVistaSprintBacklog();

        response.put("lista", lista);
        response.put("totalDatos", lista.size());

        return Response.ok(response).build();
    }
    

}
