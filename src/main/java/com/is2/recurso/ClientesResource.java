package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.ClientesService;
import com.is2.entidad.Clientes;

@Named
@RequestScoped
@Path("/rest/clientes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClientesResource extends ResourceBase<Clientes, ServiceBase<Clientes, DaoBase<Clientes>>> {

	@Inject
	private ClientesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Clientes getEntity() {
		return new Clientes();
	}

	@Override
	protected Class<Clientes> getEntityKeyType() {
		return Clientes.class;
	}
}