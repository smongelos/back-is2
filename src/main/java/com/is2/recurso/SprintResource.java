package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.SprintService;
import com.is2.entidad.Sprint;

@Named
@RequestScoped
@Path("/rest/sprint")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SprintResource extends ResourceBase<Sprint, ServiceBase<Sprint, DaoBase<Sprint>>> {

	@Inject
	private SprintService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Sprint getEntity() {
		return new Sprint();
	}

	@Override
	protected Class<Sprint> getEntityKeyType() {
		return Sprint.class;
	}
}