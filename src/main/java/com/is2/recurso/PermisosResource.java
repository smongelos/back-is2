package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.PermisosService;
import com.is2.entidad.Permisos;

@Named
@RequestScoped
@Path("/rest/permisos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PermisosResource extends ResourceBase<Permisos, ServiceBase<Permisos, DaoBase<Permisos>>> {

	@Inject
	private PermisosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Permisos getEntity() {
		return new Permisos();
	}

	@Override
	protected Class<Permisos> getEntityKeyType() {
		return Permisos.class;
	}
}