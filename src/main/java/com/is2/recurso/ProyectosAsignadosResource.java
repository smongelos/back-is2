package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.ProyectosAsignadosService;
import com.is2.entidad.ProyectosAsignados;

@Named
@RequestScoped
@Path("/rest/proyectos-asignados")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProyectosAsignadosResource extends ResourceBase<ProyectosAsignados, ServiceBase<ProyectosAsignados, DaoBase<ProyectosAsignados>>> {

	@Inject
	private ProyectosAsignadosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ProyectosAsignados getEntity() {
		return new ProyectosAsignados();
	}

	@Override
	protected Class<ProyectosAsignados> getEntityKeyType() {
		return ProyectosAsignados.class;
	}
}