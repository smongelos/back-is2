package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.ProyectosService;
import com.is2.entidad.Proyectos;

@Named
@RequestScoped
@Path("/rest/proyectos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProyectosResource extends ResourceBase<Proyectos, ServiceBase<Proyectos, DaoBase<Proyectos>>> {

	@Inject
	private ProyectosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Proyectos getEntity() {
		return new Proyectos();
	}

	@Override
	protected Class<Proyectos> getEntityKeyType() {
		return Proyectos.class;
	}
}