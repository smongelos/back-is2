package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.HistoriasUsuariosService;
import com.is2.entidad.HistoriasUsuarios;

@Named
@RequestScoped
@Path("/rest/historias-usuarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class HistoriasUsuariosResource extends ResourceBase<HistoriasUsuarios, ServiceBase<HistoriasUsuarios, DaoBase<HistoriasUsuarios>>> {

	@Inject
	private HistoriasUsuariosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public HistoriasUsuarios getEntity() {
		return new HistoriasUsuarios();
	}

	@Override
	protected Class<HistoriasUsuarios> getEntityKeyType() {
		return HistoriasUsuarios.class;
	}
}