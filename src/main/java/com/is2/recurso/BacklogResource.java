package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.BacklogService;
import com.is2.entidad.Backlog;

@Named
@RequestScoped
@Path("/rest/backlog")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BacklogResource extends ResourceBase<Backlog, ServiceBase<Backlog, DaoBase<Backlog>>> {

	@Inject
	private BacklogService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Backlog getEntity() {
		return new Backlog();
	}

	@Override
	protected Class<Backlog> getEntityKeyType() {
		return Backlog.class;
	}
}