package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.PermisosAsignadosService;
import com.is2.entidad.PermisosAsignados;

@Named
@RequestScoped
@Path("/rest/permisos-asignados")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PermisosAsignadosResource extends ResourceBase<PermisosAsignados, ServiceBase<PermisosAsignados, DaoBase<PermisosAsignados>>> {

	@Inject
	private PermisosAsignadosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public PermisosAsignados getEntity() {
		return new PermisosAsignados();
	}

	@Override
	protected Class<PermisosAsignados> getEntityKeyType() {
		return PermisosAsignados.class;
	}
}