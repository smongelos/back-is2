package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.UsuariosService;
import com.is2.entidad.Usuarios;

@Named
@RequestScoped
@Path("/rest/usuarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuariosResource extends ResourceBase<Usuarios, ServiceBase<Usuarios, DaoBase<Usuarios>>> {

	@Inject
	private UsuariosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Usuarios getEntity() {
		return new Usuarios();
	}

	@Override
	protected Class<Usuarios> getEntityKeyType() {
		return Usuarios.class;
	}
}