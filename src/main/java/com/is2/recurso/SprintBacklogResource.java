package com.is2.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.is2.util.DaoBase;
import com.is2.util.ResourceBase;
import com.is2.util.ServiceBase;
import com.is2.servicio.SprintBacklogService;
import com.is2.entidad.SprintBacklog;

@Named
@RequestScoped
@Path("/rest/sprint-backlog")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SprintBacklogResource extends ResourceBase<SprintBacklog, ServiceBase<SprintBacklog, DaoBase<SprintBacklog>>> {

	@Inject
	private SprintBacklogService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public SprintBacklog getEntity() {
		return new SprintBacklog();
	}

	@Override
	protected Class<SprintBacklog> getEntityKeyType() {
		return SprintBacklog.class;
	}
}